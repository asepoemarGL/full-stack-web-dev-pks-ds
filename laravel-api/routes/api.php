<?php

use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/test', function(){
//     return "Selamat datang di Web Service";
// });

// Route::apiResource('post','PostController')->middleware('auth:api');

// Route::get('/post', 'PostController@index');
// Route::post('/post', 'PostController@store');
// Route::get('/post/{id}', 'PostController@show');
// Route::put('/post/{id}', 'PostController@update');
// Route::delete('/post/{id}', 'PostController@destroy');


//Role
Route::apiResource('role','RoleController');

// Route::get('/role', 'RoleController@index');
// Route::post('/role', 'RoleController@store');
// Route::get('/role/{id}', 'RoleController@show');
// Route::put('/role/{id}', 'RoleController@update');
// Route::delete('/role/{id}', 'RoleController@destroy');


//comment
// Route::apiResource('comment','CommentController')->middleware('auth:api');

// Route::get('/comment', 'CommentController@index');
// Route::post('/comment', 'CommentController@store');
// Route::get('/comment/{id}', 'CommentController@show');
// Route::put('/comment/{id}', 'CommentController@update');
// Route::delete('/comment/{id}', 'CommentController@destroy');

// Route::post('auth/register','Auth\RegisterController');
Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
    ] , function(){
        
        Route::post('register','RegisterController')->name('auth.register');
        Route::post('regenerate-otp-code','RegenerateOtpCodeController')->name('auth.regenerate-otp-code');
        Route::post('verification','VerificationController')->name('auth.verification');
        Route::post('update-password','UpdatePasswordController')->name('auth.update-password');
        Route::post('login','LoginController')->name('auth.login');

});

Route::group([

    'middleware' => 'auth:api',
    // 'prefix' => 'auth'

], function () {

Route::apiResource('post','PostController');
Route::apiResource('comment','CommentController');

});
