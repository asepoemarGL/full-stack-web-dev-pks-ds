<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
// use Illuminate\Notifications\Notifiable;


class Post extends Model
{
    // use Notifiable;

    protected $fillable = ['id','title','description'];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyname()} = Str::uuid();
            }
        });
    }


}
