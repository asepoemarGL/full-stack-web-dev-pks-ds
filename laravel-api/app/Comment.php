<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


class Comment extends Model
{
    //
    protected $fillable = [
        'id','content','post_id',
    ];

    protected $primaryKey = 'id';

    protected $keyType = 'string';

    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyname()} = Str::uuid();
            }
        });
    }

    public function role()
    {
        return $this->belongsTo('App\Post');
    }
}
