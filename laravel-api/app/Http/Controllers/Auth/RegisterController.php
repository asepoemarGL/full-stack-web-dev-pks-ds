<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
         //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = User::create($allRequest);

        do {
            $random = mt_rand( 100000, 999999);
            $check = OtpCode::where('otp',$random)->first();

        } while ($check);

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id

        ]);

        //kirim email otp code ke email register

        //success save to database
        // if($user) {

            return response()->json([
                'success' => true,
                'message' => 'Data user telah dibuat',
                'data'    => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ]
            ], 200);

        // } 

        //failed save to database
        // return response()->json([
        //     'success' => false,
        //     'message' => 'Data user gagal disimpan',
        // ], 409);
    }
}
