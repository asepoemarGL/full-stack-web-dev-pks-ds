<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; //untuk proses validasi di dalam RESTful API

class PostController extends Controller
{    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table posts
        $posts = Post::latest()->get();

        // dd($posts);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Post berhasil ditampilkan',
            'data'    => $posts  
        ], 200);

    }
    
    public function show($id) // public function show($id) atau show(Request $request)
    {
        //find post by ID
        $post = Post::find($id); //$post = Post::findOrFail($id);

        if($post){
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data Post',
                'data'    => $post 
            ], 200);
        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post with id '.$id.' Not Found',
        ], 404);

    }
    
    public function store(Request $request)
    {
        //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $post = Post::create([
            'title'     => $request->title,
            'description'   => $request->description,
        ]);

        //success save to database
        if($post) {

            return response()->json([
                'success' => true,
                'message' => 'Data Post telah dibuat',
                'data'    => $post  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Post gagal disimpan',
        ], 409);

    }
    
    public function update(Request $request, $id) // bisa juga Post $post diganti $id
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $post = Post::find($id); //$id

        if($post) {

            //update post
            $post->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post with id '.$id.' Updated',
                'data'    => $post  
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post with id '.$id.' Not Found',
        ], 404);

    }
    
    public function destroy($id)
    {
        //find post by ID
        $post = Post::find($id);

        if($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post with id '.$id.' Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post with id '.$id.' Not Found',
        ], 404);
    }
}