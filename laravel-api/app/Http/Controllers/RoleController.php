<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from table roles
        $roles = Role::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Role berhasil ditampilkan',
            'data'    => $roles  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $roles = Role::create([
            'name'     => $request->name,
        ]);

        //success save to database
        if($roles) {

            return response()->json([
                'success' => true,
                'message' => 'Data Role telah dibuat',
                'data'    => $roles  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data Role gagal disimpan',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Role::find($id);

        if($roles){
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data roles',
                'data'    => $roles 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'roles with id '.$id.' Not Found',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find role by ID
        $roles = Role::find($id); //$id

        if($roles) {

            //update roles
            $roles->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'roles with id '.$id.' Updated',
                'data'    => $roles  
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'roles with id '.$id.' Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = Role::find($id);

        if($roles) {

            //delete roles
            $roles->delete();

            return response()->json([
                'success' => true,
                'message' => 'roles with id '.$id.' Deleted',
            ], 200);

        }

        //data roles not found
        return response()->json([
            'success' => false,
            'message' => 'roles with id '.$id.' Not Found',
        ], 404);
    }
}
