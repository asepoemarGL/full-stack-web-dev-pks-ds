<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get data from table comments
        $comments = Comment::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Comment berhasil ditampilkan',
            'data'    => $comments  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comments = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id,
        ]);

        //success save to database
        if($comments) {

            return response()->json([
                'success' => true,
                'message' => 'Data comment telah dibuat',
                'data'    => $comments  
            ], 200);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Data comment gagal disimpan',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comment::find($id);

        if($comments){
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'Detail Data comments',
                'data'    => $comments 
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'comments with id '.$id.' Not Found',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //set validation
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find post by ID
        $comments = Comment::find($id); //$id

        if($comments) {

            //update comments
            $comments->update([
                'content'     => $request->content,
            'post_id'   => $request->post_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comments with id '.$id.' Updated',
                'data'    => $comments  
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments with id '.$id.' Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = Comment::find($id);

        if($comments) {

            //delete comments
            $comments->delete();

            return response()->json([
                'success' => true,
                'message' => 'comments with id '.$id.' Deleted',
            ], 200);

        }

        //data comments not found
        return response()->json([
            'success' => false,
            'message' => 'comments with id '.$id.' Not Found',
        ], 404);
    }
}
